-- Set default parallel 
SET default_parallel 20;

%default input './local-input/tstat-sample.txt'
%default output '/local-output/tsat/ex1'

-- Load input data from local input directory
A = LOAD '$input' using PigStorage(' ') AS (ip_c:chararray, ….);


-- Group by client IP
B = GROUP A BY ip_c;


-- Generate the output data
C = FOREACH B GENERATE group, COUNT(A);


-- Store the output (and start to execute the script)
STORE C INTO '$output';