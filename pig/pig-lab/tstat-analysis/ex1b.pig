-- Set default parallel 
-- SET default_parallel 20;

%default input '/laboratory/tstat-sample.txt'
%default output '/local-output/tsat/ex1b'

-- Load input data from local input directory
A = LOAD '$input' using PigStorage(' ') AS (
ip_c:chararray,
port_c:int,
packets_c:int,
rst_c:int,
ack_c:int,
purack_c:int,
unique_bytes_c:long,
data_pkts_c:int,
data_bytes_c:long,
rexmit_pkts_c:int,
rexmit_bytes_c:long,
out_seq_pkts_c:int,
syn_c:int,
fin_c:int,
ws_c:int,
ts_c:int,
window_scale_c:int,
sack_req_c:int,
sack_sent_c:int,
mss_c:int,
max_seg_size_c:int,
min_seg_size_c:int,
win_max_c:int,
win_min_c:int,
win_zero_c:int,
cwin_max_c:long,
cwin_min_c:long,
initial_cwin_c:long,
average_rtt_c:double,
rtt_min_c:double,
rtt_max_c:double,
stdev_rtt_c:double,
rtt_count_c:int,
ttl_min_c:int,
ttl_max_c:int,
rtx_RTO_c:int,
rtx_FR_c:int,
reordering_c:int,
net_dup_c:int,
unknown_segments_c:int,
flow_control_c:int,
unnece_rtx_rto_c:int,
unnece_rtx_fr_c:int,
different_syn_seqno_c:int,
ip_s:chararray);

ip = FOREACH A GENERATE ip_c, ip_s

-- Group by client IP
ip_c = GROUP A BY ip_c;

-- Group by server IP
ip_s = GROUP A BY ip_s;

-- count the number of TCP connection per each IP, irrespective if client or server IP
ips = UNION ip_c, ip_s;


-- Generate the output data
C = FOREACH ips GENERATE group, COUNT(A);


-- Store the output (and start to execute the script)
STORE C INTO '$output';